<?php

namespace App\Service;

class OMDbAPI
{
    private $API_Key = "31595ce6";
    
    public function getMovieByID($omdb_id)
    {
        $request_addr = "http://www.omdbapi.com/?apikey=".$this->API_Key."&i=".$omdb_id;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $request_addr);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_COOKIESESSION, true);

        $return = curl_exec($curl);

        curl_close($curl);

        return $return;
    }

    public function getMovieByTitle($title) {
        $request_addr = "http://www.omdbapi.com/?apikey=".$this->API_Key."&s=".$title;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $request_addr);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_COOKIESESSION, true);

        $return = curl_exec($curl);

        curl_close($curl);

        return $return;
    }
}