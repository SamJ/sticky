<?php
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Note;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class NoteController extends AbstractController {

    /**
     * @Route ("/note", name="note_list")
     */
    public function list() {
        $notes = $this->getDoctrine()
            ->getRepository(Note::class)
            ->findAll();
        return $this->render('note/note.html.twig', ['notes' => $notes]);
    }

    /**
     * @Route ("/note/new", name="note_new")
     */
    public function new(Request $request) {

        $note = new Note();

        $form = $this->createFormBuilder($note)
            ->add('name', TextType::class)
            ->add('info', TextType::class)
            ->add('save', SubmitType::class, ['label' => 'Create note'])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$note` variable has also been updated
            $note = $form->getData();

            // ... perform some action, such as saving the note to the database
            // for example, if Note is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($note);
            $entityManager->flush();

            return $this->redirectToRoute('note_list');
        }

        return $this->render('note/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route ("/note/{id}", name="note_show")
     */
    public function show($id) {
        $note = $this->getDoctrine()
            ->getRepository(Note::class)
            ->find($id);

        if (!$note) {
            throw $this->createNotFoundException(
                'No note found for id '.$id
            );
        }

        // or render a template
        // in the template, print things with {{ product.name }}
        return $this->render('note/show.html.twig', ['note' => $note]);
    }
}