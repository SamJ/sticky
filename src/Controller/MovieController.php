<?php
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Movie;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\HttpFoundation\Request;
use App\Service\OMDbAPI;
use Symfony\Component\HttpFoundation\JsonResponse;

class MovieController extends AbstractController {

    /**
     * @Route ("/movie", name="movie_list")
     */
    public function list(Request $request, OMDbAPI $API_Service) {
        $movies = $this->getDoctrine()
            ->getRepository(Movie::class)
            ->findAll();

        if ($request->request->get('value')) {
            $value = $request->request->get('value');
            $movieId = $request->request->get('movieId');
            $jsonData = array();
            $jsonData["post"] = $_POST;

            $entityManager = $this->getDoctrine()->getManager();
            $movie = $entityManager->getRepository(Movie::class)->find($movieId);
        
            if (!$movie) {
                $movie_array = json_decode($API_Service->getMovieByID($movieId), true);
                $movie = new Movie();
                $movie->setOmdbId($movie_array["imdbID"]);
                $movie->setTitle($movie_array["Title"]);
                $entityManager->persist($movie);
            }
            if ($value == "notSeen") {
                $movie->setNotSeen(true);
                $movie->setToSee(false);
                $movie->setSeen(false);
            } else if ($value == "toSee") {
                $movie->setNotSeen(false);
                $movie->setToSee(true);
                $movie->setSeen(false);
            } else {
                $movie->setNotSeen(false);
                $movie->setToSee(false);
                $movie->setSeen(true);
            }
            
            $entityManager->flush();

            $movie = $entityManager->getRepository(Movie::class)->find($movieId);

            $jsonData["movieId"] = $movie->getOmdbId();
            $jsonData["value"] = $movie->getSeen() ? "seen" : ($movie->getNotSeen() ? "notSeen" : "toSee");
            
            return new JsonResponse($jsonData);
        } else {
            return $this->render('movie/movie.html.twig', ['movies' => $movies]);
        }

        

    }

    /**
     * @Route ("/movie/new", name="movie_new")
     */
    public function new(Request $request, OMDbAPI $API_Service) {

        $movie_list = array();

        $form = $this->createFormBuilder()
            ->add('title', SearchType::class)
            ->add('save', SubmitType::class, ['label' => 'Search'])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $title = implode($form->getData());

            $movies_array = json_decode($API_Service->getMovieByTitle($title), true)["Search"];
            
            // var_dump($movies_array);
            foreach($movies_array as $movie) {
                $movie_obj = new Movie();
                $movie_from_db = $this->getDoctrine()
                    ->getRepository(Movie::class)
                    ->findOneByTitle($movie["Title"]);
                if ($movie_from_db) {
                    $movie_obj = $movie_from_db;
                    // $movie_obj->setSeen($movie_from_db->getSeen());
                    // $movie_obj->setNotSeen($movie_from_db->getNotSeen());
                    // $movie_obj->setToSee($movie_from_db->getToSee());
                } else {
                    $movie_obj->setTitle($movie["Title"]);
                    $movie_obj->setOmdbId($movie["imdbID"]);
                }
                
                $movie_list[] = $movie_obj;
            }
            

            return $this->render('movie/new.html.twig', [
                'form' => $form->createView(),
                'movies' => $movie_list
            ]);
        }

        return $this->render('movie/new.html.twig', [
            'form' => $form->createView(),
            'movies' => $movie_list
        ]);
    }

    /**
     * @Route ("/movie/{id}", name="movie_show")
     */
    public function show(Request $request, $id, OMDbAPI $API_Service) {

        $movie = $this->getDoctrine()
            ->getRepository(Movie::class)
            ->find($id);

        if (!$movie) {
            $movie_array = json_decode($API_Service->getMovieByID($id), true);
            $movie = new Movie();
            $movie->setOmdbId($movie_array["imdbID"]);
            $movie->setTitle($movie_array["Title"]);
        }

        $form = $this->createFormBuilder($movie)
            ->add('notSeen', SubmitType::class, [
                'label' => 'NOT SEEN', 
                'attr'   =>  array(
                    'class'   => ($movie->getNotSeen() ? 'btn_green' : 'btn_blue'))
                ])
            ->add('toSee', SubmitType::class, [
                'label' => 'TO SEE',
                'attr'   =>  array(
                    'class'   => ($movie->getToSee() ? 'btn_green' : 'btn_blue'))
                ])
            ->add('seen', SubmitType::class, [
                'label' => 'SEEN',
                'attr'   =>  array(
                    'class'   => ($movie->getSeen() ? 'btn_green' : 'btn_blue'))
                ])
            ->getForm();
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $movie = $form->getData();
            if ($form->get('toSee')->isClicked()) {
                $movie->setToSee(true);
                $movie->setNotSeen(false);
                $movie->setSeen(false);
            } else if ($form->get('seen')->isClicked()) {
                $movie->setToSee(false);
                $movie->setNotSeen(false);
                $movie->setSeen(true);
            } else if ($form->get('notSeen')->isClicked()) {
                $movie->setToSee(false);
                $movie->setNotSeen(true);
                $movie->setSeen(false);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($movie);
            $entityManager->flush();

            return $this->redirectToRoute('movie_show', ['id' => $movie->getOmdbId()]);
        }

        return $this->render('movie/show.html.twig', ['movie' => $movie, 'form' => $form->createView()]);
    }
}