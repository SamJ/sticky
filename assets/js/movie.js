console.log("movie");

var movies = document.getElementsByClassName("movieButton");
movies.forEach(element => {
    element.addEventListener("click", handleMovieButton);
});

function handleMovieButton(event) {
    console.log(event.target.value);
    console.log(event.target.dataset.id);

    var data = new FormData();
    data.append("movieId", event.target.dataset.id);
    data.append("value", event.target.value);

    fetch('/movie', {
            method: 'POST',
            body: data
        })
        .then(response => response.text())
        .catch(error => console.error('Error:', error))
        .then(response => {
            response = JSON.parse(response)
            console.log('Success');
            var buttons = event.target.parentNode.getElementsByTagName("button");
            buttons.forEach(element => {
                if (element.value == response["value"]) {
                    element.classList.add("btn_green");
                    element.classList.remove("btn_blue");
                } else {
                    element.classList.add("btn_blue");
                    element.classList.remove("btn_green");
                }
            })
        });

};