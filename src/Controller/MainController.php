<?php
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Note;
use App\Entity\Task;
use App\Entity\Movie;


class MainController extends AbstractController {
    /**
     * @Route ("/", name="home")
     */
    public function home() {
        $notes = $this->getDoctrine()
            ->getRepository(Note::class)
            ->findAll();
        $tasks = $this->getDoctrine()
            ->getRepository(Task::class)
            ->findAll();
        $movies = $this->getDoctrine()
            ->getRepository(Movie::class)
            ->findAll();
        return $this->render('home.html.twig', ['notes' => $notes, 'tasks' => $tasks, 'movies' => $movies]);
    }
}