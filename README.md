Sticky is a web application that allow you to quickly save something from your 
mind : take a note, set up a task to do later, put a reminder for a rendez-vous,
save a film to watch later...

Its based on the symfony framework and its connected to the OMdB API to search
film info.

![alt text](Screenshot.PNG "Screenshot")

