<?php
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Task;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class TaskController extends AbstractController {

    /**
     * @Route ("/task", name="task_list")
     */
    public function list() {
        $tasks = $this->getDoctrine()
            ->getRepository(Task::class)
            ->findAll();
        return $this->render('task/task.html.twig', ['tasks' => $tasks]);
    }

    /**
     * @Route ("/task/new", name="task_new")
     */
    public function new(Request $request) {

        $task = new Task();

        $form = $this->createFormBuilder($task)
            ->add('name', TextType::class)
            ->add('dueDate', DateType::class)
            ->add('info', TextType::class)
            ->add('save', SubmitType::class, ['label' => 'Create Task'])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $task = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($task);
            $entityManager->flush();

            return $this->redirectToRoute('task_list');
        }

        return $this->render('task/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route ("/task/{id}", name="task_show")
     */
    public function show($id) {
        $task = $this->getDoctrine()
            ->getRepository(Task::class)
            ->find($id);

        if (!$task) {
            throw $this->createNotFoundException(
                'No task found for id '.$id
            );
        }

        // or render a template
        // in the template, print things with {{ product.name }}
        return $this->render('task/show.html.twig', ['task' => $task]);
    }
}