<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MovieRepository")
 */
class Movie
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=255)
     */
    private $omdb_id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $seen;

        /**
     * @ORM\Column(type="boolean")
     */
    private $toSee;

         /**
     * @ORM\Column(type="boolean")
     */
    private $notSeen;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $year;

    private $summary;


    public function getOmdbId(): ?string
    {
        return $this->omdb_id;
    }

    public function setOmdbId(string $omdb_id): self
    {
        $this->omdb_id = $omdb_id;

        return $this;
    }

    public function getSeen(): ?string
    {
        return $this->seen;
    }

    public function setSeen(?string $seen): self
    {
        $this->seen = $seen;

        return $this;
    }

    public function getToSee(): ?string
    {
        return $this->toSee;
    }

    public function setToSee(?string $toSee): self
    {
        $this->toSee = $toSee;

        return $this;
    }

    public function getNotSeen(): ?string
    {
        return $this->notSeen;
    }

    public function setNotSeen(?string $notSeen): self
    {
        $this->notSeen = $notSeen;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(?int $year): self
    {
        $this->year = $year;

        return $this;
    }
}
